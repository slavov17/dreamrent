from django.urls import path
from . import views
from django.conf.urls import url

urlpatterns = [
    url(r'^register-user/$', views.register_user, name='register-user'),
    url(r'^login-user/$', views.login_user, name='login-user'),
    url(r'^logout/$', views.disconnect_user, name='logout'),
    url(r'^verify-user/$', views.verify_user, name='verify-user'),
    url(r'^explore/$', views.explore, name='explore'),
    url(r'^dashboard/$', views.dashboard, name='dashboard'),
    url(r'^get-tags/$', views.get_tags, name='get-tags'),
    url(r'^save-tag/$', views.save_tag, name='save-tag'),
    url(r'^create-site/$', views.create_site, name='create-site'),
    url(r'^edit-user/$', views.edit_user, name='edit-user'),
    url(r'^cancel-reservation/$', views.cancel_reservation, name='cancel-reservation'),
    url(r'^site-preview/(?P<id>\d+)/$', views.site_preview, name='site-preview'),
    url(r'^search/$', views.search, name='search'),
    url(r'^get-pricerange/$', views.get_pricerange, name='get-pricerange'),
    url(r'^delete-site/(?P<id>\d+)/$', views.delete_site, name='delete-site')

    # url(r'google902f260a0fcb68dd.html', views.check_google, name='save-tag'),

]
