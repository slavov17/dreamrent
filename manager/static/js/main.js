
    var close = document.getElementsByClassName("closebtn");
    var i;

    for (i = 0; i < close.length; i++) {
        close[i].onclick = function () {
            var div = this.parentElement;
            div.style.opacity = "0";
            setTimeout(function () {
                div.style.display = "none";
            }, 600);
        }
    }

	function TagManager() {

		this.tagList = [];

		this.uploadFormTags = jQuery( '.property-tags-block' );

		var self = this;

		this.getTagsUrl = window.location.origin + "/get-tags";

		this.saveTagUrl = window.location.origin + "/save-tag/";

		this.getTags = function (term) {
			return Requester.get(self.getTagsUrl + "?term=" + term);
		};

		this.saveTag = function (tag) {
			var data = {
				term: tag
			};
			return Requester.post(self.saveTagUrl, data);
		};
	}

	var Requester = (function () {

		var obj = {};
		obj.csrf_token = jQuery("[name='csrf_token']").attr("content");
		obj.get = function (url) {
			return jQuery.ajax({
				type: "GET",
				url: url
			});
		};
		obj.post = function (url, data) {
			data.csrfmiddlewaretoken = obj.csrf_token;
			return jQuery.ajax({
				type: "POST",
				url: url,
				data: data
			});
		};

		return obj;
	})();

	function SiteManager() {

		this.tagManager = new TagManager();

		this.autocomplete = function () {

			var self = this;
			var locationTagElemens = jQuery("#location-tags");
			locationTagElemens.autocomplete({
				source: function (request, response) {

					var term = request.term;
					self.tagManager.getTags(term).done(function (result) {

						// If no results found, create a new tag
						if(result.length == 0) {
							console.log("Tag does not exist.")
							return false;
						}

						var arr = [];
						jQuery.each(result, function(index, value){
							var obj = {
								label: value.name,
								value: value.name,
							};

							arr.push( obj );
						});
						response(arr);
					});
				},
				select: function (event, ui) {

					console.log(locationTagElemens)
					locationTagElemens.trigger(
						jQuery.Event( 'keypress', { keyCode: 13, which: 13 } )
					);
				}
			});
		};
	}

    function SearchManager() {

        this.el = jQuery("#searchform");

        this.args = function() {

            var obj = {};

            var term = this.el.find("[name='s']").val();
            var type = this.el.find("[name='type']").val();
            var latLon = (typeof this.el.find("[name='geolocation']").attr("content") != 'undefined')?JSON.parse(this.el.find("[name='geolocation']").attr("content")):{lat:"", lon:""};
			var tags = jQuery("[name='tags']").val();
			var closetome = this.el.find("[name='closetome']").data("closetome");
			var pricefrom = jQuery("[name='pricefrom']").val();
			var priceto = jQuery("[name='priceto']").val();
			obj.s = (typeof term != 'undefined') ? term : "";
            obj.type = (typeof type != 'undefined') ? type : "";
            obj.tags = (typeof tags != 'undefined') ? tags : JSON.stringify([]);
            obj.lat = (typeof latLon != 'undefined') ? latLon.lat : "";
            obj.lon = (typeof latLon != 'undefined') ? latLon.lon : "";
            obj.closetome = (typeof closetome != 'undefined') ? closetome : 0; // 0/1
            obj.pricefrom = (typeof pricefrom != 'undefined') ? pricefrom : "";
            obj.priceto = (typeof priceto != 'undefined') ? priceto : "";
            return obj;
        };

        this.getQueryUrl = function() {

            var string = "";
            var i  = 0;
            var args = this.args();

            for(var a in args) {
                if(i == 0)
                    string += "?" + a + "=" + args[a];
                else
                    string += "&" + a + "=" + args[a];
                i++;
            }

			var returnUrl = window.location.origin + "/search" + string;
			if(window.location.href.match(/(\?|\&)/g)) {
				returnUrl = window.location.href;
			}
            return returnUrl;
        };

        this.sideContainer = jQuery(".locations-results .results-list");

        this.getSideLocationHtml = function(data) {

            return `
                <li class="location-item location-ajax" data-id="${data.id}">
                	<div class="image">
                		<a target="_blank" href="${window.location.origin + "/site-preview/" + data.id}">
                			<img width="150" height="135" src="${window.location.origin + "/media/" + data.featured_image}" class="attachment-tt_explore_locations size-tt_explore_locations wp-post-image" alt="${data.title}">								</a>
                	</div>

                	<div class="item-body">
                		<h4 class="item-title">
                			<a target="_blank" href="${window.location.origin}/site-preview/${data.id}">${data.title}</a>
                		</h4>
						<p class="location">${data.address}</p>
                		<ul class="clean-list item-facilities">
                		    ${data.tags}
                		</ul>
                	</div>
                </li>
            `;
        };
	}



	jQuery(document).ready(function (jQuery) {


	});
