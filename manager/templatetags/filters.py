from django import template
from datetime import datetime
from datetime import timedelta
register = template.Library()
from datetime import date
from manager.models import SiteType, Site




@register.filter(is_safe=True)
def verified(a):
    if a.verified:
        return False
    else:
        return True


@register.filter(is_safe=True)
def get_cat_qty(a):
    return len(Site.objects.all().filter(site_type = SiteType.objects.get(name=a)))
