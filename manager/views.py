from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
from DreamRent import settings
from .models import User
import os
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
import time
from django.contrib.auth import login, logout
import random
import string
from django.contrib.auth import authenticate, login
from threading import Thread
from django.core.mail import send_mail
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from manager.models import Reservation, Site, SiteTag, SiteType, Picture
from django.http import JsonResponse
from django.db.models import Q, F, FloatField
from django.contrib.auth import update_session_auth_hash
# from  django.contrib.gis import geoip2
from django.contrib.gis.geoip2.base import GeoIP2
from functools import reduce
import geopy
from geopy import distance
from django.contrib.gis.geoip2 import GeoIP2

import operator

stamp = lambda: int(round(time.time() * 1000))


def generate_unique_url():
    return ''.join((random.choice(string.ascii_uppercase + string.digits)) for x in range(1, 200))


def email(request, subject, to, text_content, html_content):
    from_email = settings.EMAIL_HOST
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()


def send_activation_email(request, user):
    subject = "DreamRent please verify your email"
    plain = "Hello, " + user.first_name + " please visit " + settings.HOST_NAME + user.verification_email + ", so we can confirm your email."
    html = '<html>' + \
           "<p>Hello, " + user.first_name + " please click " + "<a href=" + settings.HOST_NAME + user.verification_email + ">here</a>" + ", so we can confirm your email.</p>" + \
           '<html>'
    email(request, subject, user.email, plain, html)


def home(request):
    context = {'locations': json.dumps(list(Site.objects.all().values())),
               'tags': SiteTag.objects.all(),
               'types': SiteType.objects.all(),
               'categories': SiteType.objects.all()}

    return render(request, 'home.html', context)


def register_user(request):
    r = request.POST
    try:
        print(User.objects.get(email=r['email']))
        messages.error(request, 'An user with this email is already registered')
        return HttpResponseRedirect('/')
    except:

        user = User.objects.create_user(
            first_name=r['first_name'],
            last_name=r['last_name'],
            email=r['email'],
            password=r['password'],
            username=r['email'],
            verification_email='verify-user?confirm=' + generate_unique_url()

        )
        user.save()

        t = Thread(target=send_activation_email, args=(request, user), )
        t.daemon = True
        t.start()

        login(request, user, backend='django.contrib.auth.backends.ModelBackend')

    return HttpResponseRedirect('/')


def login_user(request):
    username = request.POST['email']
    password = request.POST['password']
    user = authenticate(username=username, password=password)

    if user is None:
        messages.warning(request, "Wrong credentials")
        return HttpResponseRedirect('/')
    else:
        login(request, user)
        return HttpResponseRedirect('/')


def disconnect_user(request):
    logout(request)
    return HttpResponseRedirect('/')


def verify_user(request):
    if request.GET.get('confirm'):
        confirm = request.GET.get('confirm')
        try:
            user = User.objects.get(verification_email='verify-user?confirm=' + confirm)
            user.verification_email = ''
            user.verified = True
            user.save()
            messages.success(request, 'Your account has been validated')
            return HttpResponseRedirect('/')
        except:
            return HttpResponseRedirect('/')



    else:
        return HttpResponseRedirect('/')


import json


def explore(request):
    g = GeoIP2()

    try:
        print(get_client_ip(request))
        print(g.country(get_client_ip(request)))
        print(g.city(get_client_ip(request)))

    except:
        pass
    context = {'locations': json.dumps(list(Site.objects.all().values())),
               'tags': SiteTag.objects.all(),
               'types': SiteType.objects.all(),
               'locationsnojson':Site.objects.all()}
    # JSON}

    return render(request, 'explore.html', context)


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def dashboard(request):
    context = {}
    if request.user.is_authenticated:
        context = {'reservations': Reservation.objects.all().filter(guest=request.user),
                   'sites': Site.objects.all().filter(owner=request.user),
                   'profile': request.user,
                   'categories': SiteType.objects.all()
                   }
        print(SiteType.objects.all())
    return render(request, 'dashboard.html', context)


def get_tags(request):
    if request.method == 'GET':
        term = request.GET.get('term')
        queryset = SiteTag.objects.all().filter(name__icontains=term).values()
        return JsonResponse(list(queryset), safe=False)
    return HttpResponse('none')


def save_tag(request):
    if request.method == 'POST':
        term = request.POST['term']
        try:
            SiteTag.objects.get(name=term)
        except:
            sitetag = SiteTag.objects.create(
                name=term
            )
            sitetag.save()
        return HttpResponse('')
    return HttpResponse('none')


def site_preview(request, id):
    site = Site.objects.get(id=id)
    context = {'site': site,
               'images': Picture.objects.filter(site=site),
               }
    # NORMA<}
    return render(request, 'site-preview.html', context)


def delete_site(request, id):
    if request.user.is_authenticated and Site.objects.all().get(id=id).owner == request.user:
        Site.objects.get(id=id).delete()
    return HttpResponse('ok')






def search(request):
    if request.method == "GET":
        r = request.GET
        string = r['s']
        site_type = r['type']
        t = r.getlist('tags')
        lat = r['lat']
        long = r['lon']
        closetome = r['closetome']

        if closetome == 1:
            my_coordinates = (lat, long)
            g = GeoIP2()
            ip = get_client_ip(request)
            if ip != '127.0.0.1':
                my_coordinates = (g.coords(ip))

            sites = Site.objects.all()
            for i in sites:
                coordinates = (i.long, i.lat)
                if calc_distance(coordinates, my_coordinates) > settings.AROUNDME_MAX:
                    sites = sites.exclude(id=i.id)

            for i in sites:
                i['marker'] = Picture.objects.filter(site=Site.objects.get(id=i['id'])).values()[0]
                i['marker_coord'] = {
                    "lat": i['lat'],
                    "lon": i['long']
                }
                i['tags'] = list(Site.objects.get(id=i['id']).tags.values_list('name', flat=True))
            return JsonResponse(list(sites), safe=False)

        query = reduce(operator.or_, (Q(title__icontains=string), ~Q(tags__name=t)))

        sites = Site.objects.all().filter(query).filter(site_type__name=site_type).values()
        print(sites)
        for i in sites:
            i['marker'] = Picture.objects.filter(site=Site.objects.get(id=i['id'])).values()[0]
            i['marker_coord'] = {
                "lat": i['lat'],
                "lon": i['long']
            }
            i['tags'] = list(Site.objects.get(id=i['id']).tags.values_list('name', flat=True))
            if len(list(sites)) ==0:
                sites = Site.objects.all()
                print(sites)
                for i in sites:
                    i['marker'] = Picture.objects.filter(site=Site.objects.get(id=i['id'])).values()[0]
                    i['marker_coord'] = {
                        "lat": i['lat'],
                        "lon": i['long']
                    }
                    i['tags'] = list(Site.objects.get(id=i['id']).tags.values_list('name', flat=True))
        return JsonResponse(list(sites), safe=False)


def get_pricerange(request):
    if request.method == "GET":
        r = request.GET
        price_from = r['pricefrom']
        price_to = r['priceto']
        if price_from == '':
            price_from = 0
        if price_to == '':
            price_to = 1000

        sites = Site.objects.all().filter(price__gte=float(price_from), price__lte=float(price_to)).values()
        return JsonResponse(list(sites), safe=False)


def calc_distance(c1, c2):
    return geopy.distance.VincentyDistance(c1, c2).km


def create_site(request):
    if request.user.is_authenticated:
        p = request.POST
        print(request.POST)
        orders = p['location-images-order']
        orders = orders.replace('[', '').replace(']', '').split(',')

        print('futured_image', p['location-featured-image'])

        site = Site.objects.create(
            owner=request.user,
            title=p['location-name'],
            address=p['location-address'],
            price=p['location-price'],
            site_type=SiteType.objects.get(name=p['location-category']),
            long=p['location-coordinates[long]'],
            lat=p['location-coordinates[lat]'],

        )

        for tag in p['location-tags'].split(','):

            try:
                site.tags.add(SiteTag.objects.get(name=str(tag)))
            except:
                site.tags.create(name=str(site))

        # site.save()
        # MUST ADD PICS
        futured = request.POST['location-featured-image']

        files = request.FILES.getlist('location-images[]')

        if futured == '':
            futured = files[0]
        count = 0

        for file in files:
            if str(file) == str(futured):
                site.featured_image = file
                site.save()
            else:
                pic = Picture.objects.create(pic=file, site=site, ordering=int(orders[count]))
                pic.save()
            count += 1
        messages.success(request, 'Successfully created site')
        return HttpResponseRedirect('/dashboard')
    return HttpResponseRedirect('/')


def edit_user(request):
    if request.user.is_authenticated:
        user = request.user
        r = request.POST
        ignored = ['csrfmiddlewaretoken', 'wp-submit', 'password', 'password']
        user.first_name = request.POST['firstname']
        user.last_name = request.POST['lastname']
        user.country = request.POST['country']
        user.phonenumber = request.POST['phonenumber']
        user.city = request.POST['city']
        user.address = request.POST['address']
        user.save()
        messages.success(request, 'Successfully edited profile')
        return HttpResponseRedirect('/dashboard')
    return HttpResponseRedirect('/')


#######
##TO DO EDIT PROFILE AND SVE EDITED USER

##TO DO CANCEL RESERVATRIOSN
def cancel_reservation(request):
    if request.user.is_authenticated:
        user = request.user
        res_id = request.POST['reservation']
        reservation = Reservation.objects.get(id=res_id)
        if reservation:
            reservation.booked = False
            reservation.save()

## CREATE SITE PREVIEW CLIENT SINGLE PAGE


# def check_google(request):
#     return render(request, 'google902f260a0fcb68dd.html')
