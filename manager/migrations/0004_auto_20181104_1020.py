# Generated by Django 2.0 on 2018-11-04 10:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('manager', '0003_auto_20181104_0732'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitetype',
            name='icon',
            field=models.ImageField(blank=True, default=None, null=True, upload_to='icons/'),
        ),
        migrations.AddField(
            model_name='sitetype',
            name='picture',
            field=models.ImageField(blank=True, default=None, null=True, upload_to='icons/'),
        ),
    ]
